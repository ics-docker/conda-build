conda-build
===========

Docker_ image based on registry.esss.lu.se/ics-docker/centos-miniconda3 to build conda_ packages.

The image includes:

- Development tools
- conda-build 3


Building
--------

This image is built automatically by gitlab-ci.

How to use this image
---------------------

To create a conda package, you must write a conda build recipe: https://conda.io/docs/user-guide/tutorials/build-pkgs.html


.. _Docker: https://www.docker.com
.. _conda: https://conda.io/docs/index.html
